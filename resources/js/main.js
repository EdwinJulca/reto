const url = 'https://restcountries.eu/rest/v2/lang/es';

this.listaPaises = [];
this.salida = '';

fetch(url, {
    method: 'GET',
}).then(rs => rs.json()).then(listaPaises => {
    listaPaises.length = 12

    for (var i = 0; i < listaPaises.length; i++) {
        salida +=            `
            <div class="infocardContainer">
                <div id="main">
                    <img  src="${listaPaises[i].flag}"></img>
                </div>
                <div id="textbois">
                    <a href="#openModal"> <h2>${listaPaises[i].name}</h2></a>
                    <h4>Capital: ${listaPaises[i].capital}</h4>
                    <h4>Population: ${listaPaises[i].population}</h4>
                    <h4>Coin: ${listaPaises[i].currencies[0].name}</h4>
                </div>
            </div>
            <div id="openModal" class="modalDialog">
                <div>
                    <a href="#close" title="Close" class="close">X</a>
                    <h2>${listaPaises[i].name}</h2>
                    <h4><b>CONTINENT</b>: ${listaPaises[i].region}</h4>
                    <h4>SUBCONTINENT: ${listaPaises[i].subregion}</h4>
                    <div class="modal-detail"></div>
                </div>
            </div>
         `;
    }

    document.getElementById('paises').innerHTML = salida;
    console.log(listaPaises)
}).catch(err => console.log(err));